# Excess deaths in Europe: The data

This dataset on excess deaths across Europe during the coronavirus pandemic has been collated by [Newsworthy](https://www.newsworthy.se) in collaboration with [EDJNet](https://europeandatajournalism.eu/) for a story on the regional differences in covid-19 deaths. 

Read the full first story here: https://medium.com/p/778e546765a9/

And find regional analysis for 500 regions here: https://www.newsworthy.se/sv/corona-excess-deaths/

Updated stories with over 750 regions were published in August: https://medium.com/newsworthy-se/most-european-regions-back-to-normal-death-rates-several-regions-in-sweden-still-above-9abcffabb31a 

And in December: 

You are free to republish all stories and visualisations under Creative Commons, in line with the [syndication guidelines](https://www.europeandatajournalism.eu/eng/Syndication).

## About the data

The data was first published on 17 June 2020 and updated on 24 August 2020 and 9 December 2020. It is collected from Eurostat and national statistical agencies. (Scotland: NRS, Northern Ireland: NISRA, Germany: Destatis). 

We have gathered as geographically granular data as possible (mostly at NUTS3 level), but in countries such as Germany only NUTS1 level data has been published. A number of countries in Central and Eastern Europe have not reported any regional statistics on excess. These are excluded from this analysis.

Excess deaths have been calculated by comparing all deaths reported in a region since the start of the pandemic with the average number of deaths during that time period in the previous couple of years. We've also done seasonal analysis, looking at just spring (March-May), summer (June-Aug), and autumn (Sep-Nov).

For most countries, the average period is 2015–2019. Others have fewer years of data available, but at least two full years have been used. Some, like Spain, have modelled the expected number of deaths for 2020 instead of providing historical figures.

A region is defined as having had excess deaths if reported deaths were at least 5 percent higher and 20 more than expected. If deaths were at least 25 percent higher than expected, we have defined it as a region with "significant excess".

The data contains the following columns:

* `region_code`: the region's NUTS code            
* `region_name`
* `country`               
* `year`
* `week`
* `deaths_during_pandemic`: the total number of deaths that have been reported in the region since the start of the pandemic
* `avg_deaths`: the average number of deaths reported in the same time period in previous years
* `excess_deaths_count_total` / `excess_deaths_count_spring` / `excess_deaths_count_summer` / `excess_deaths_count_autumn`: reported deaths minus average deaths since the start of the pandemic / March-May / June-Aug / Sep-Nov
* `excess_deaths_percent_total` / `excess_deaths_percent_spring` / `excess_deaths_percent_summer` / `excess_deaths_percent_autumn`: the above expressed as a percentage
* `country_excess_rank_total` / `country_excess_rank_spring` / `country_excess_rank_summer` / `country_excess_rank_autumn`: how a region's excess deaths ranks within its country since the start of the pandemic / March-May / June-Aug / Sep-Nov
* `europe_excess_rank_total` / `europe_excess_rank_spring` / `europe_excess_rank_summer`/ `europe_excess_rank_autumn`: how a region's excess deaths ranks compared to all regions since the start of the pandemic / March-May / June-Aug / Sep-Nov

## Contact

E-mail clara@newsworthy.se if you have any questions.


